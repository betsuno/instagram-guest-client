<?php namespace InstagramClient\helpers;

class ArrayHelper
{
	/**
	 * @param array $array
	 * @param array|string $path
	 * @param mixed $defaultValue
	 * @return mixed
	 */
	public static function getValue(&$array, $path, $defaultValue = null)
	{
		$path = (array) $path;
		$result = $array;
		foreach ($path as $key)
		{
			if (!isset($result[$key])) {
				return $defaultValue;
			}
			$result = $result[$key];
		}
		return $result;
	}
}