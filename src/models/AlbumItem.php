<?php namespace InstagramClient\models;

/**
 * Class AlbumItem
 * @package InstagramClient\models
 *
 * @method bool hasId()
 * @method bool hasIsVideo()
 * @method bool hasThumbnailUrl()
 * @method bool hasVideoUrl()
 * @method int getId()
 * @method string getIsVideo()
 * @method string getThumbnailUrl()
 * @method string getVideoUrl()
 */
class AlbumItem extends GenericModel
{
	protected $propertiesMap = [
		'id'                        => 'int',
		'is_video'                  => 'bool',
		'thumbnail_url'             => 'string',
		'video_url'                 => 'string',
	];
}
