<?php namespace InstagramClient\models;

/**
 * Class Media
 * @package InstagramClient\models
 *
 * @method bool     hasId()
 * @method bool     hasLikesCount()
 * @method bool     hasComments()
 * @method bool     hasCreatedAt()
 * @method bool     hasOwner()
 * @method bool     hasText()
 * @method int      getId()
 * @method int      getLikesCount()
 * @method array    getComments()
 * @method int      getCreatedAt()
 * @method User     getOwner()
 * @method string   getText()
 *
 */
class Comment extends GenericModel
{
	protected $propertiesMap = [
		'id'                        => 'int',
        'likes_count'               => 'int',
        'comments'                  => 'arr',
        'created_at'                => 'int',
        'owner'                     => User::class,
        'text'                      => 'string',
	];
}
