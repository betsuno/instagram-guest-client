<?php namespace InstagramClient\models;

/**
 * Class UserItem
 * @package InstagramClient\models
 *
 * @method bool hasId()
 * @method bool hasUsername()
 * @method bool hasFullName()
 * @method bool hasProfilePictureUrl()
 * @method bool hasIsPrivate()
 * @method int getId()
 * @method string getUsername()
 * @method string getFullName()
 * @method string getProfilePictureUrl()
 * @method bool getIsPrivate()
 */
class UserItem extends GenericModel
{
	protected $propertiesMap = [
		'id'                  => 'int',
		'username'            => 'string',
		'full_name'           => 'string',
		'profile_picture_url' => 'string',
		'is_private'          => 'bool',
	];
}