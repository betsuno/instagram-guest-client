<?php
require __DIR__ . '/../vendor/autoload.php';

$client = new \InstagramClient\Client();
$cursor = null;
do {
    $response = $client->getUserFeed(3410133831, $cursor);

    foreach ($response->getItems() as $item) {
        print $item->getType() . PHP_EOL;

        if ($item->getType() === \InstagramClient\models\Media::ALBUM_TYPE) {
            foreach ($item->getAlbumItems() as $slide) {
                print '->' . $slide->getId() . PHP_EOL;
            }
        }
    }

    $cursor = $response->getCursor();
} while(!empty($cursor));