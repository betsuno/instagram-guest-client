<?php namespace InstagramClient\responses;

use InstagramClient\exceptions\FormatException;
use InstagramClient\helpers\ArrayHelper;
use InstagramClient\models\Hashtag;
use InstagramClient\models\User;

/**
 * Class TopSearchResponse
 * @package InstagramClient\responses
 */
class TopSearchResponse extends BaseResponse
{
    /** @var User[] */
    private $users = [];
    /** @var Hashtag[] */
    private $hashtags = [];

    /**
     * TopSearchResponse constructor.
     * @param array $attributes
     * @throws FormatException
     */
    public function __construct($attributes)
    {
        parent::__construct($attributes);

        if (!isset($attributes['users']) || !is_array($attributes['users'])) {
            throw new FormatException('');
        }

        $this->users = array_map(function (array $item) {
            $user = ArrayHelper::getValue($item, 'user');
            return new User([
                'id'                  => ArrayHelper::getValue($user, ['pk']),
                'username'            => ArrayHelper::getValue($user, ['username']),
                'full_name'           => ArrayHelper::getValue($user, ['full_name']),
                'profile_picture_url' => ArrayHelper::getValue($user, ['profile_pic_url']),
                'followers_count'     => ArrayHelper::getValue($user, ['follower_count']),
                'is_private'          => ArrayHelper::getValue($user, ['is_private']),
                'is_verified'         => ArrayHelper::getValue($user, ['is_verified']),
            ]);
        }, $attributes['users']);

        $this->hashtags = array_map(function (array $item) {
            $tag = ArrayHelper::getValue($item, 'hashtag');
            return new Hashtag([
                'id'                     => ArrayHelper::getValue($tag, ['id']),
                'name'                   => ArrayHelper::getValue($tag, ['name']),
                'media_count'            => ArrayHelper::getValue($tag, ['media_count']),
                'use_default_avatar'     => ArrayHelper::getValue($tag, ['use_default_avatar']),
                'profile_pic_url'        => ArrayHelper::getValue($tag, ['profile_pic_url']),
                'search_result_subtitle' => ArrayHelper::getValue($tag, ['search_result_subtitle']),
            ]);
        }, $attributes['hashtags']);
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return Hashtag[]
     */
    public function getHashtags()
    {
        return $this->hashtags;
    }
}
