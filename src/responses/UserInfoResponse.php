<?php namespace InstagramClient\responses;

use InstagramClient\models\User;

/**
 * Class UserInfoResponse
 * @package InstagramClient\responses
 */
class UserInfoResponse extends User
{
	public function __construct($response)
	{
		$data = $response['graphql']['user'];

		parent::__construct([
			'id'                  => $data['id'],
			'username'            => $data['username'],
			'full_name'           => $data['full_name'],
			'biography'           => $data['biography'],
			'profile_picture_url' => $data['profile_pic_url'],
			'profile_picture_id'  => User::extractProfilePictureIdFromUrl($data['profile_pic_url']),
			'external_link'       => $data['external_url'],
			'media_count'         => $data['edge_owner_to_timeline_media']['count'],
			'followers_count'     => $data['edge_followed_by']['count'],
			'follows_count'       => $data['edge_follow']['count'],
			'is_private'          => $data['is_private'],
			'is_business'         => $data['is_business_account'],
			'is_verified'         => $data['is_verified'],
            'reel_count'          => isset($data['highlight_reel_count']) ? $data['highlight_reel_count'] : null,
		]);
	}


	public static function createFromPrivate($response)
	{
		$data = $response['user'];
		return new parent([
			'id'                  => $data['pk'],
			'username'            => $data['username'],
			'full_name'           => $data['full_name'],
			'biography'           => $data['biography'],
			'profile_picture_url' => $data['profile_pic_url'],
			'external_link'       => isset($data['external_url']) ? $data['external_url'] : null,
			'media_count'         => $data['media_count'],
			'followers_count'     => $data['follower_count'],
			'follows_count'       => $data['following_count'],
			'is_private'          => $data['is_private'],
			'is_verified'         => $data['is_verified'],
		]);
	}
}