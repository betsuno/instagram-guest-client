<?php
require __DIR__ . '/../vendor/autoload.php';

$client = new \InstagramClient\Client();

$cursor = null;
do {
    print '>>> ' . $cursor . PHP_EOL . PHP_EOL;
    $response = $client->getMediaCommentsByCode("CFZ6ZV2sFZX", $cursor);

    foreach ($response->getItems() as $comment) {
        print $comment->getOwner()->getUsername() . ': ' . $comment->getText() . ' (' . $comment->getLikesCount() . ')' . PHP_EOL;
        print date('Y-m-d H:i:s', $comment->getCreatedAt()) . PHP_EOL . PHP_EOL;
    }

    print '-------------' . PHP_EOL . PHP_EOL;

    if (!$response->hasNextPage()) {
        break;
    }

    $cursor = $response->getCursor();
} while (!empty($cursor));