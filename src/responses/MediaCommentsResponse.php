<?php namespace InstagramClient\responses;

use InstagramClient\components\PaginateResponse;
use InstagramClient\helpers\ArrayHelper;
use InstagramClient\models\Comment;
use InstagramClient\models\User;
use InstagramClient\models\UserItem;

/**
 * Class MediaCommentsResponse
 * @package InstagramClient\responses
 *
 * @method Comment[] getItems()
 */
class MediaCommentsResponse extends BaseResponse
{
	use PaginateResponse;

	/**
	 * @param $attributes
	 */
	public function __construct($attributes)
	{
		parent::__construct($attributes);
		$data = $attributes['data']['shortcode_media']['edge_media_to_parent_comment'];

        $items = array_map(function ($item) {
            $data = $item['node'];

            $owner = ArrayHelper::getValue($data, ['owner'], []);

            if (isset($owner['profile_pic_url'])) {
                $owner['profile_picture_url'] = $owner['profile_pic_url'];
                unset($owner['profile_pic_url']);
            }

            $likes = ArrayHelper::getValue($data, ['edge_liked_by'], []);
            return [
                'id'                        => ArrayHelper::getValue($data, ['id']),
                'likes_count'               => ArrayHelper::getValue($likes, ['count'], 0),
                'comments'                  => ArrayHelper::getValue($data, ['edge_threaded_comments'], []),
                'created_at'                => ArrayHelper::getValue($data, ['created_at']),
                'owner'                     => $owner,
                'text'                      => ArrayHelper::getValue($data, ['text']),
            ];
        }, ArrayHelper::getValue($data, ['edges'], []));

		$this->setItems($items, Comment::class);
		$this->setTotal($data['count']);
		$this->setCursor($data['page_info']['end_cursor']);
		$this->setHasNextPage($data['page_info']['has_next_page']);
	}
}