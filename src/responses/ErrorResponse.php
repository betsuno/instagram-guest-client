<?php namespace InstagramClient\responses;

/**
 * Class ErrorResponse
 * @package InstagramClient\responses
 */
class ErrorResponse extends BaseResponse
{
	/** @var string|null */
	protected $message;

	/** @var integer|null */
	protected $code;

	/**
	 * @param $attributes
	 * @return void
	 */
	public function __construct($attributes)
	{
		parent::__construct($attributes);

		$this->message = $attributes['message'];
		$this->code = $attributes['code'];
	}

	/**
	 * @return bool
	 */
	public function hasMessage()
	{
		return !is_null($this->message);
	}

	/**
	 * @return bool
	 */
	public function hasCode()
	{
		return !is_null($this->code);
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @return int
	 */
	public function getCode()
	{
		return $this->code;
	}
}