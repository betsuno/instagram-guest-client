<?php namespace InstagramClient\responses;

use InstagramClient\Client;

/**
 * Class BaseResponse
 * @package InstagramClient\responses
 */
class BaseResponse
{
	/** @var string|null */
	protected $status = null;

	/**
	 * BaseResponse constructor.
	 * @param $attributes
	 */
	public function __construct($attributes)
	{
		$this->status = $attributes['status'];
	}

	/**
	 * @return bool
	 */
	public function isOk()
	{
		return $this->status === Client::OK_STATUS;
	}

	/**
	 * @return bool
	 */
	public function isError()
	{
		return $this->status === Client::ERROR_STATUS;
	}
}