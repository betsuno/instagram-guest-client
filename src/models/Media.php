<?php namespace InstagramClient\models;

use InstagramClient\helpers\ArrayHelper;

/**
 * Class Media
 * @package InstagramClient\models
 *
 * @method bool hasId()
 * @method bool hasCode()
 * @method bool hasCaption()
 * @method bool hasType()
 * @method bool hasCommentsCount()
 * @method bool hasLikesCount()
 * @method bool hasIsCaptionEdited()
 * @method bool hasThumbnailUrl()
 * @method bool hasHighResolutionImageUrl()
 * @method bool hasLink()
 * @method bool hasShortCode()
 * @method bool hasCreatedAt()
 * @method bool hasVideoUrl()
 * @method bool hasOwner()
 * @method bool hasAlbum()
 * @method int getId()
 * @method string getCode()
 * @method string getCaption()
 * @method string getType()
 * @method int getCommentsCount()
 * @method int getLikesCount()
 * @method bool isCaptionEdited()
 * @method string getThumbnailUrl()
 * @method string getHighResolutionImageUrl()
 * @method string getLink()
 * @method string getShortCode()
 * @method int getCreatedAt()
 * @method string getVideoUrl()
 * @method User getOwner()
 * @method AlbumItem[] getAlbumItems()
 */
class Media extends GenericModel
{
    const IMAGE_TYPE = 'image';
    const VIDEO_TYPE = 'video';
    const ALBUM_TYPE = 'album';
    const REEL_TYPE = 'reel';

	protected $propertiesMap = [
		'id'                        => 'int',
		'code'                      => 'string',
        //string
		'caption'                   => 'arr',
		'type'                      => 'string',
		'comments_count'            => 'int',
        'comments'                  => 'arr',
		'likes_count'               => 'int',
		'is_caption_edited'         => 'bool',
		'thumbnail_url'             => 'string',
		'high_resolution_image_url' => 'string',
		'link'                      => 'string',
		'short_code'                => 'string',
		'created_at'                => 'int',
		'video_url'                 => 'string',
        'album_items'               => 'arr',
		'owner'                     => User::class,
        'geotags'                   => 'arr',
	];

    /**
     * @param $media
     * @return string
     */
	public static function extractType($media)
    {
        $type = Media::IMAGE_TYPE;

        if (isset($media['edge_sidecar_to_children'])) {
            $type = Media::ALBUM_TYPE;
        }

        if ($media['is_video']) {
            $type = Media::VIDEO_TYPE;
        }

        if (ArrayHelper::getValue($media, ['type'], Media::IMAGE_TYPE) === Media::REEL_TYPE) {
            $type = Media::REEL_TYPE;
        }

        return $type;
    }

    /**
     * @param $media
     * @return array
     */
    public static function extractSlides($media)
    {
        $slides = [];
        if (isset($media['edge_sidecar_to_children'])) {
            foreach ($media['edge_sidecar_to_children']['edges'] as $edge)
            {
                $slides[] = new AlbumItem([
                    'id'            => $edge['node']['id'],
                    'is_video'      => $edge['node']['is_video'],
                    'thumbnail_url' => $edge['node']['display_url'],
                    'video_url'     => $edge['node']['is_video'] ? $edge['node']['video_url'] : null,
                ]);
            }
        }

        return $slides;
    }

	/**
	 * @param string $id
	 *
	 * @return string
	 */
	public static function getCodeFromId($id)
	{
		$parts = explode('_', $id);
		$id = $parts[0];
		$alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
		$code = '';
		while ($id > 0) {
			$remainder = $id % 64;
			$id = ($id - $remainder) / 64;
			$code = $alphabet[$remainder] . $code;
		}
		return $code;
	}
}
