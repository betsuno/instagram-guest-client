<?php namespace InstagramClient\models;

class GenericModel
{
	/** @var array */
	protected $_attributes = [];

	/** @var array */
	protected $propertiesMap = [];

	/**
	 * BaseModel constructor.
	 * @param array $attributes
	 */
	public function __construct($attributes = [])
	{
		if (empty($this->propertiesMap)) {
			$this->_attributes = $attributes;
			return;
		}

		foreach ($attributes as $attribute => $value) {
			if (isset($this->propertiesMap[$attribute]) && !is_null($value))
			{
				switch ($this->propertiesMap[$attribute]) {
					case('int'):
						$this->_attributes[$attribute] = intval($value);
						continue 2;

					case('string'):
						$this->_attributes[$attribute] = strval($value);
						continue 2;

					case('bool'):
						$this->_attributes[$attribute] = boolval($value);
						continue 2;

                    case('arr'):
                        $this->_attributes[$attribute] = is_array($value) ? $value : [];
                        continue 2;
				}

				if (class_exists($this->propertiesMap[$attribute])) {
					$class = $this->propertiesMap[$attribute];
					$this->_attributes[$attribute] = new $class($value);
				}
			}
		}
	}

	/**
	 * @param $method
	 * @param array $args
	 * @return bool|mixed|null
	 * @throws \Exception
	 */
	public function __call($method, $args = [])
	{
		preg_match('/(has|get|is)([A-Za-z]*)/', $method, $matches);

		if (count($matches) < 3) {
			throw new \Exception('Unknown method');
		}

		list($methodName, $action, $attribute) = $matches;
		$attribute = self::camelCaseToUnderscore($attribute);

		switch ($action) {
			case('has'):
				return $this->hasAttribute($attribute);

			case('get'):
				return $this->getAttribute($attribute, null);

			case('is'):
				return $this->getAttribute($attribute, null);
		}

		throw new \Exception("Unknown method ${$methodName}");
	}

	/**
	 * @param $string
	 * @return string
	 */
	private static function camelCaseToUnderscore($string)
	{
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
		$ret = $matches[0];
		foreach ($ret as &$match) {
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}
		return implode('_', $ret);
	}

	/**
	 * @param $attribute
	 * @return bool
	 */
	protected function hasAttribute($attribute)
	{
		return isset($this->_attributes[strtolower($attribute)]);
	}

	/**
	 * @param $attribute
	 * @param $defaultValue
	 * @return bool
	 */
	protected function getAttribute($attribute, $defaultValue = null)
	{
		return $this->hasAttribute($attribute) ? $this->_attributes[strtolower($attribute)] : $defaultValue;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return $this->_attributes;
	}
}