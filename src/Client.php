<?php namespace InstagramClient;

use Exception;
use InstagramClient\components\SharedData;
use InstagramClient\exceptions\BadResponseException;
use InstagramClient\exceptions\FormatException;
use InstagramClient\models\Media;
use InstagramClient\models\User;
use InstagramClient\responses\HashtagFeedResponse;
use InstagramClient\responses\LocationFeedResponse;
use InstagramClient\responses\MediaCommentsResponse;
use InstagramClient\responses\MediaInfoResponse;
use InstagramClient\responses\MediaLikesResponse;
use InstagramClient\responses\TopSearchResponse;
use InstagramClient\responses\UserFeedResponse;
use InstagramClient\responses\UserInfoResponse;
use InstagramClient\responses\UserTaggedFeedResponse;

class Client
{
	const OK_STATUS = 'ok';
	const ERROR_STATUS = 'error';

	private $cookies = [];
	private $rhxGis;
	private $csrfToken;

	private $lastResponse;

	private $proxy;

	private static function instagramUseragentsList()
	{
		return [
			'Mozilla/5.0 (Linux; Android 8.1.0; motorola one Build/OPKS28.63-18-3; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.80 Mobile Safari/537.36 Instagram 72.0.0.21.98 Android (27/8.1.0; 320dpi; 720x1362; motorola; motorola one; deen_sprout; qcom; pt_BR; 132081645)',
			'Mozilla/5.0 (Linux; Android 6.0.1; SM-J700M Build/MMB29K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/70.0.3538.110 Mobile Safari/537.36 Instagram 73.0.0.22.185 Android (23/6.0.1; 320dpi; 720x1280; samsung; SM-J700M; j7elte; samsungexynos7580; pt_BR; 133633069)',
			'Mozilla/5.0 (Linux; Android 6.0.1; ZUK Z2131 Build/MMB29M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/48.0.2564.106 Mobile Safari/537.36 Instagram 62.0.0.19.93 Android (23/6.0.1; 480dpi; 1080x1920; ZUK; ZUK Z2131; z2_plus; qcom; pt_BR; 123790722)',
			'Mozilla/5.0 (Linux; Android 7.0; SM-G610M Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/69.0.3497.100 Mobile Safari/537.36 Instagram 65.0.0.12.86 Android (24/7.0; 480dpi; 1080x1920; samsung; SM-G610M; on7xelte; samsungexynos7870; es_US; 126223536)',
			'Mozilla/5.0 (Linux; Android 8.1.0; SM-J530G Build/M1AJQ; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/69.0.3497.100 Mobile Safari/537.36 Instagram 66.0.0.11.101 Android (27/8.1.0; 320dpi; 720x1280; samsung; SM-J530G; j5y17lte; samsungexynos7870; pt_BR; 127049016)',
			'Mozilla/5.0 (Linux; Android 7.0; SM-G610M Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/69.0.3497.91 Mobile Safari/537.36 Instagram 62.0.0.19.93 Android (24/7.0; 480dpi; 1080x1920; samsung; SM-G610M; on7xelte; samsungexynos7870; pt_BR; 123790722)',
		];
	}

	private static function getRandomInstagramUseragent()
	{
		$list = static::instagramUseragentsList();
		$key = array_rand($list);
		return $list[$key];
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function handleRequestToPage($request)
	{
		$this->lastResponse = $request->send(Request::GET_METHOD);

		$this->setCookies($this->lastResponse->getCookies());
		$sharedData = SharedData::createFromContent($this->lastResponse->getBody());

		if ($sharedData instanceof SharedData) {
			if ($sharedData->hasRhxGis()) {
				$this->rhxGis = $sharedData->getRhxGis();
			}

			if ($sharedData->hasCsrfToken()) {
				$this->csrfToken = $sharedData->getCsrfToken();
			}
		}

		return $this->lastResponse;
	}

	/**
	 * @param Request $request
	 * @return Response
	 */
	public function handleXhrRequest($request)
	{
		$request->withHeader('x-requested-with', 'XMLHttpRequest');

		$this->lastResponse = $request->send(Request::GET_METHOD);

		$this->setCookies($this->lastResponse->getCookies());
		$this->csrfToken = $this->getCookie('csrftoken', $this->csrfToken);

		return $this->lastResponse;
	}

	/**
	 * @param $proxy
	 */
	public function setProxy($proxy)
	{
		$this->proxy = $proxy;
	}

	/**
	 * @return string|null
	 */
	public function getProxy()
	{
		return $this->proxy;
	}

	/**
	 * @param $path
	 * @return Request
	 */
	public function makeRequest($path)
	{
		return (new Request(Endpoints::getBaseEndpoint() . $path))
			->withCookies($this->getCookies())
			->withProxy($this->getProxy());
	}

    /**
     * @param $instagram_id
     * @return User
     * @throws Exception
     */
	public function getUserInfoById($instagram_id)
	{
		$path = Endpoints::getUserInfoById($instagram_id);
		$request = $this->makeRequest($path)
			->setUrl(Endpoints::getPrivateEndpoint() . $path);
		$request->withHeader('user-agent', static::getRandomInstagramUseragent());
		$data = json_decode($this->handleRequestToPage($request)->getBody(), true);

		if (!$data || !isset($data['user'], $data['user']['username'])) {
			return null;
		}

		return $this->getUserInfo($data['user']['username']);
	}

	/**
	 * @param $username
	 * @return User
	 * @throws Exception
	 */
	public function getUserInfo($username)
	{
		$path = Endpoints::getUserInfo($username);
		$request = $this->makeRequest($path);

		if ($this->hasRhxGis()) {
			$request
				->withHeader('x-instagram-gis', $this->generateGisTokenFromPath($path))
				->setUrl(Endpoints::getBaseEndpoint() . $path . '?__a=1');

			$data = json_decode($this->handleXhrRequest($request)->getBody(), true);

			if (!$data) {
				return null;
			}
		} else {
			$sharedData = SharedData::createFromContent($this->handleRequestToPage($request)->getBody());

			if (is_null($sharedData)) {
				return null;
			}

			$entryData = $sharedData->getEntryData();

			if ($entryData && isset($entryData['ProfilePage'], $entryData['ProfilePage'][0])) {
				$data = $entryData['ProfilePage'][0];
			} else {
				return null;
			}
		}

		return new UserInfoResponse($data);
	}

	/**
	 * @param $id
	 * @param string|null $cursor
	 * @return UserFeedResponse
	 */
	public function getUserFeed($id, $cursor = null)
	{
		$variables = [
			'id'    => strval($id),
			'first' => strval(12),
			'after' => strval($cursor)
		];

		$variables = json_encode($variables);
		$path = Endpoints::getUserFeed($variables);
		$request = $this->makeRequest($path);

		if (!$this->getCookie('csrftoken')) {
			$this->getRemoteRhxGis();
		}

		$request->withHeader('x-csrftoken', $this->getCookie('csrftoken'));
		$response = $this->handleXhrRequest($request);

		return new UserFeedResponse(json_decode($response->getBody(), true));
	}

    /**
     * @param $id
     * @return UserTaggedFeedResponse
     */
    public function getUserTaggedFeed($id)
    {
        $variables = [
            'id'    => strval($id),
            'first' => strval(12),
        ];

        $variables = json_encode($variables);
        $path = Endpoints::getUserTaggedFeed($variables);
        $request = $this->makeRequest($path);

        if (!$this->getCookie('csrftoken')) {
            $this->getRemoteRhxGis();
        }

        $request->withHeader('x-csrftoken', $this->getCookie('csrftoken'));
        $response = $this->handleXhrRequest($request);

        return new UserTaggedFeedResponse(json_decode($response->getBody(), true));
    }

	/**
	 * @param $tagName
	 * @param string|null $cursor
	 * @return HashtagFeedResponse
	 */
	public function getHashtagFeed($tagName, $cursor = null)
	{
		$variables = [
			'tag_name' => mb_strtolower(strval(trim($tagName))),
			'first'    => 50,
			'after'    => strval($cursor)
		];

		$variables = json_encode($variables, JSON_UNESCAPED_UNICODE);

		$path = Endpoints::getHashtagFeed(urlencode($variables));
		$request = $this->makeRequest($path);

		if (!$this->hasRhxGis()) {
			$this->getRemoteRhxGis();
		}

		$request->withHeader('x-csrftoken', $this->getCookie('csrftoken'));
		$response = $this->handleXhrRequest($request);

		return new HashtagFeedResponse(json_decode($response->getBody(), true));
	}

	/**
	 * @param $location_id
	 * @param string|null $cursor
	 * @return LocationFeedResponse
	 * @throws Exception
	 */
	public function getLocationFeed($location_id, $cursor = null)
	{
		$variables = [
			'id'    => strval(intval($location_id)),
			'first' => 50,
			'after' => strval($cursor)
		];

		$variables = json_encode($variables, JSON_UNESCAPED_UNICODE);

		$path = Endpoints::getLocationFeed($variables);
		$request = $this->makeRequest($path);

		if (!$this->hasRhxGis()) {
			$this->getRemoteRhxGis();
		}

		$request->withHeader('x-instagram-gis', $this->generateGisTokenFromVariables($variables));
		$response = $this->handleXhrRequest($request);

		$result = json_decode($response->getBody(), true);

		if (!isset($result['data'], $result['data']['location']) || empty($result['data']['location'])) {
			throw new Exception('Location not found', 404);
		}

		return new LocationFeedResponse($result);
	}

	/**
	 * @param $id
	 * @return MediaInfoResponse
	 */
	public function getMediaInfo($id)
	{
		$path = Endpoints::getMediaInfo(Media::getCodeFromId($id));
		$request = $this->makeRequest($path);

		if (!$this->hasRhxGis()) {
			$this->getRemoteRhxGis();
		}

		$request->withHeader('x-instagram-gis', $this->generateGisTokenFromPath($path));
		$response = $this->handleXhrRequest($request);

		return new MediaInfoResponse(json_decode($response->getBody(), true));
	}

    /**
     * @param $id
     * @param null $after
     * @param null $limit
     * @return MediaLikesResponse
     */
	public function getMediaLikes($id, $after = null, $limit = null)
	{
		return $this->getMediaLikesByCode(Media::getCodeFromId($id), $after, $limit);
	}

    /**
     * @param $code
     * @param null $after
     * @param null $limit
     * @return MediaLikesResponse
     */
	public function getMediaLikesByCode($code, $after = null, $limit = null)
	{
		$variables = [
			'shortcode'    => $code,
			'include_reel' => true,
			'first'        => $limit === null ? ($after ? 12 : 24) : $limit,
		];
		if ($after && $limit !== null) {
			$variables['after'] = $after;
		}

		$variables = json_encode($variables, JSON_UNESCAPED_UNICODE);
		$path = Endpoints::getMediaLikes($variables);
		$request = $this->makeRequest($path);

		if (!$this->hasRhxGis()) {
			$this->getRemoteRhxGis();
		}
		$request->withHeader('x-instagram-gis', $this->generateGisTokenFromVariables($variables));
		$request->withHeader('referrer', sprintf('https://www.instagram.com/p/%s/', $code));

		$response = $this->handleXhrRequest($request);

		return new MediaLikesResponse(json_decode($response->getBody(), true));
	}

    /**
     * @param $code
     * @param null $after
     * @param null $limit
     * @return MediaCommentsResponse
     */
    public function getMediaCommentsByCode($code, $after = null, $limit = null)
    {
        $variables = [
            'shortcode'    => $code,
            'include_reel' => true,
            'first'        => $limit === null ? ($after ? 12 : 50) : $limit,
        ];

        if (!is_null($after)) {
            $variables['after'] = $after;
        }

        $variables = json_encode($variables);

        $request = $this->makeRequest(Endpoints::getMediaComments($variables));

        if (!$this->hasRhxGis()) {
            $this->getRemoteRhxGis();
        }

        $request->withHeader('x-instagram-gis', $this->generateGisTokenFromVariables($variables));
        $request->withHeader('referrer', sprintf('https://www.instagram.com/p/%s/', $code));

        $response = $this->handleXhrRequest($request);

        return new MediaCommentsResponse(json_decode($response->getBody(), true));
    }

    /**
     * @param string $query
     * @return TopSearchResponse
     * @deprecated Название сбивает с толку, т.к. ищем одним запросом не только пользователей
     * @see Client::search()
     * @throws BadResponseException
     */
    public function searchUsers(string $query)
    {
        $path = Endpoints::topSearch($query);
        $request = $this->makeRequest($path);
        $response = $this->handleXhrRequest($request);
        try {
            return new TopSearchResponse(json_decode($response->getBody(), true));
        } catch (FormatException $e) {
            throw new BadResponseException('Bad response', 0, $e);
        }
    }

    /**
     * @param string $query
     * @return TopSearchResponse
     * @throws BadResponseException
     */
    public function search(string $query)
    {
        $path = Endpoints::topSearch($query);
        $request = $this->makeRequest($path);
        $response = $this->handleXhrRequest($request);
        try {
            return new TopSearchResponse(json_decode($response->getBody(), true));
        } catch (FormatException $e) {
            throw new BadResponseException('Bad response', 0, $e);
        }
    }

	/**
	 * @param array $cookies
	 * @return void
	 */
	public function setCookies($cookies = [])
	{
		$this->cookies = $cookies;
	}

	/**
	 * @return array
	 */
	public function getCookies()
	{
		return $this->cookies;
	}

	/**
	 * @param $cookieName
	 * @return bool
	 */
	public function hasCookie($cookieName)
	{
		return isset($this->getCookies()[$cookieName]);
	}

	/**
	 * @param $cookieName
	 * @param null $defaultValue
	 * @return mixed|null
	 */
	public function getCookie($cookieName, $defaultValue = null)
	{
		return $this->hasCookie($cookieName) ? $this->getCookies()[$cookieName] : $defaultValue;
	}

	/**
	 * @return bool
	 */
	public function hasRhxGis()
	{
		return !empty($this->rhxGis);
	}

	/**
	 * @return mixed|null
	 */
	public function getRhxGis()
	{
		return $this->rhxGis;
	}

	/**
	 * @return string|null
	 */
	public function getRemoteRhxGis()
	{
		$response = $this->handleRequestToPage($this->makeRequest('/'));
		$sharedData = SharedData::createFromContent($response->getBody());
		if ($sharedData === null) {
			return null;
		}
		return $sharedData->getRhxGis();
	}

	/**
	 * @param $variables
	 * @return string
	 */
	public function generateGisTokenFromVariables($variables)
	{
		return md5(implode(':', [$this->getRhxGis(), $variables]));
	}

	/**
	 * @param $path
	 * @return string
	 */
	public function generateGisTokenFromPath($path)
	{
		return md5(implode(':', [$this->getRhxGis(), $path]));
	}
}