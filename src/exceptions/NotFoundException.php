<?php namespace InstagramClient\exceptions;

class NotFoundException extends InstagramException {}