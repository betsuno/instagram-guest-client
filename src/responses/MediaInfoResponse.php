<?php namespace InstagramClient\responses;

use InstagramClient\models\Media;

/**
 * Class MediaInfoResponse
 * @package InstagramClient\responses
 */
class MediaInfoResponse extends Media
{
	public function __construct($response)
	{
		$data = $response['graphql']['shortcode_media'];

		parent::__construct([
			'id'                        => $data['id'],
			'caption'                   => isset($data['edge_media_to_caption']['edges'][0]['node']['text']) ? $data['edge_media_to_caption']['edges'][0]['node']['text'] : null,
			'type'                      => Media::extractType($data),
			'comments_count'            => $data['edge_media_to_parent_comment']['count'],
			'comments'                  => $data['edge_media_to_parent_comment']['edges'],
			'likes_count'               => $data['edge_media_preview_like']['count'],
			'is_caption_edited'         => null,
			'thumbnail_url'             => $data['display_resources'][0]['src'],
			'high_resolution_image_url' => $data['display_url'],
			'link'                      => sprintf('https://www.instagram.com/p/%s/', $data['shortcode']),
			'short_code'                => $data['shortcode'],
			'created_at'                => $data['taken_at_timestamp'],
			'video_url'                 => isset($data['video_url']) ? $data['video_url'] : null,
			'owner'                     => $data['owner'],
            'album_items'               => Media::extractSlides($data)
		]);
	}
}