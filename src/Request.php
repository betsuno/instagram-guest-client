<?php namespace InstagramClient;
use Exception;

class Request
{
    const GET_METHOD = 'GET';
    const POST_METHOD = 'POST';

    public $url;
    public $headers = [];
    public $cookies = [];
    public $proxy = null;
    public $method;
    public $userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.106 Safari/537.36';

    /**
     * Request constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
        $this->withHeaders($this->getDefaultHeaders());
    }

    /**
     * @return array
     */
    private function getDefaultHeaders()
    {
        return [
            'user-agent' => $this->getUserAgent()
        ];
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param string $userAgent
     * @return Request
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @param $proxy
     * @return Request
     */
    public function withProxy($proxy)
    {
        $this->proxy = $proxy;

        return $this;
    }

    /**
     * @param $header
     * @param $value
     * @return Request
     */
    public function withHeader($header, $value)
    {
        $this->headers[$header] = $value;

        return $this;
    }

    /**
     * @param array $headers
     * @return Request
     */
    public function withHeaders($headers = [])
    {
        foreach ($headers as $header => $value) {
            $this->withHeader($header, $value);
        }

        return $this;
    }

    /**
     * @param $cookieName
     * @param $value
     * @return Request
     */
    public function withCookie($cookieName, $value)
    {
        $this->cookies[$cookieName] = $value;

        return $this;
    }

    /**
     * @param array $cookies
     * @return Request
     */
    public function withCookies($cookies = [])
    {
        foreach ($cookies as $cookieName => $value) {
            $this->withCookie($cookieName, $value);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param $key
     * @param $val
     * @return string
     */
    private static function getHeaderString($key, $val)
    {
        $key = trim(strtolower($key));
        return $key . ': ' . $val;
    }

    /**
     * @return array
     */
    private function getFormattedHeaders()
    {
        $formattedHeaders = [];

        $combinedHeaders = array_change_key_case($this->getHeaders());

        foreach ($combinedHeaders as $key => $val) {
            $formattedHeaders[] = self::getHeaderString($key, $val);
        }

        return $formattedHeaders;
    }

    /**
     * @param string $method
     * @param array $data
     * @return Response
     * @throws Exception
     */
    public function send($method = self::GET_METHOD, $data = [])
    {
        $ch = curl_init();

        if (count($this->cookies) > 0) {
            $cookies = '';
            foreach ($this->cookies as $key => $value) {
                $cookies .= "$key=$value; ";
            }

            $this->withHeader('cookie', $cookies);
        }

        curl_setopt_array($ch, [
            CURLOPT_URL            => $this->getUrl(),
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER         => true,
            CURLOPT_VERBOSE        => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS      => 3,
            CURLOPT_HTTPHEADER     => $this->getFormattedHeaders(),
            CURLOPT_TIMEOUT        => 15,
        ]);

        if (!is_null($this->proxy)) {
            curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        }

        $response_raw = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch));
        }
        $response = Response::createFromCurlResponse($response_raw, $ch);

        curl_close($ch);

        return $response;
    }
}