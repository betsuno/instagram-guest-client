<?php namespace InstagramClient\models;

/**
 * Class User
 * @package InstagramClient\models
 *
 * @method bool hasId()
 * @method bool hasUsername()
 * @method bool hasFullName()
 * @method bool hasBiography()
 * @method bool hasProfilePictureUrl()
 * @method bool hasProfilePictureId()
 * @method bool hasExternalUrl()
 * @method bool hasMediaCount()
 * @method bool hasFollowsCount()
 * @method bool hasFollowersCount()
 * @method bool hasIsPrivate()
 * @method bool hasIsVerified()
 * @method bool hasReelCount()
 * @method int getId()
 * @method string getUsername()
 * @method string getFullName()
 * @method string getBiography()
 * @method string getProfilePictureUrl()
 * @method int getProfilePictureId()
 * @method string getExternalUrl()
 * @method int getMediaCount()
 * @method int getFollowsCount()
 * @method int getFollowersCount()
 * @method bool getIsPrivate()
 * @method bool getIsVerified()
 * @method int getReelCount()
 */
class User extends GenericModel
{
    /**
     * @var string[]
     */
	protected $propertiesMap = [
		'id'                  => 'int',
		'username'            => 'string',
		'full_name'           => 'string',
		'biography'           => 'string',
		'profile_picture_url' => 'string',
		'profile_picture_id'  => 'int',
		'external_link'       => 'string',
		'media_count'         => 'int',
		'followers_count'     => 'int',
		'follows_count'       => 'int',
		'is_private'          => 'bool',
		'is_business'         => 'bool',
		'is_verified'         => 'bool',
		'reel_count'          => 'int',
	];

    /**
     * @param $url
     * @return string|null
     */
	public static function extractProfilePictureIdFromUrl($url)
    {
        preg_match_all('/\/(\d*)_(\d*)_(\d*)_n.jpg/m', $url, $matches, PREG_SET_ORDER, 0);

        if (isset($matches[0])) {
            return sprintf('%s_%s_%s', $matches[0][1], $matches[0][2], $matches[0][3]);
        }

        return null;
    }
}