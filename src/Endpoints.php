<?php namespace InstagramClient;

/**
 * Class Endpoints
 * @package InstagramClient
 */
class Endpoints
{
	const DEFAULT_ENDPOINT = 'https://www.instagram.com';
	const PRIVATE_ENDPOINT = 'https://i.instagram.com';

	const USER_INFO_ENDPOINT = '/{username}/';
	const USER_FEED_ENDPOINT = '/graphql/query/?query_hash=f2405b236d85e8296cf30347c9f08c2a&variables={variables}';
	const HASHTAG_FEED_ENDPOINT = '/graphql/query/?query_hash=f12c9ec5e46a3173b2969c712ad84744&variables={variables}';
	const LOCATION_FEED_ENDPOINT = '/graphql/query/?query_hash=ac38b90f0f3981c42092016a37c59bf7&variables={variables}';
    const MEDIA_COMMENTS_ENDPOINT = '/graphql/query/?query_hash=bc3296d1ce80a24b1b6e40b1e72903f5&variables={variables}';
	const MEDIA_INFO_ENDPOINT = '/p/{code}/?__a=1';
	const MEDIA_LIKES_ENDPOINT = '/graphql/query/?query_hash=e0f59e4a1c8d78d0161873bc2ee7ec44&variables={variables}';
	const USER_TAGGED_ENDPOINT = '/graphql/query/?query_hash=31fe64d9463cbbe58319dced405c6206&variables={variables}';
	const USER_INFO_BY_ID_ENDPOINT = '/api/v1/users/{instagramId}/info/';
	const TOP_SEARCH_ENDPOINT = '/web/search/topsearch/?query={query}';

	/**
	 * @param $username
	 * @return mixed
	 */
	public static function getUserInfo($username)
	{
		return str_replace(['{username}'], [$username], self::USER_INFO_ENDPOINT);
	}

	/**
	 * @param $variables
	 * @return mixed
	 */
	public static function getUserFeed($variables)
	{
		return str_replace(['{variables}'], [$variables], self::USER_FEED_ENDPOINT);
	}

    /**
     * @param $variables
     * @return mixed
     */
    public static function getUserTaggedFeed($variables)
    {
        return str_replace(['{variables}'], [$variables], self::USER_TAGGED_ENDPOINT);
    }

	/**
	 * @param $instagram_id
	 * @return mixed
	 */
	public static function getUserInfoById($instagram_id)
	{
		return str_replace(['{instagramId}'], [$instagram_id], self::USER_INFO_BY_ID_ENDPOINT);
	}

	/**
	 * @param $variables
	 * @return mixed
	 */
	public static function getHashtagFeed($variables)
	{
		return str_replace(['{variables}'], [$variables], self::HASHTAG_FEED_ENDPOINT);
	}

	/**
	 * @param $variables
	 * @return mixed
	 */
	public static function getLocationFeed($variables)
	{
		return str_replace(['{variables}'], [$variables], self::LOCATION_FEED_ENDPOINT);
	}

	/**
	 * @param $mediaId
	 * @return mixed
	 */
	public static function getMediaInfo($mediaId)
	{
		return str_replace(['{code}'], [$mediaId], self::MEDIA_INFO_ENDPOINT);
	}

	/**
	 * @return string
	 */
	public static function getBaseEndpoint()
	{
		return self::DEFAULT_ENDPOINT;
	}

	/**
	 * @return string
	 */
	public static function getPrivateEndpoint()
	{
		return self::PRIVATE_ENDPOINT;
	}

	/**
	 * @param $variables
	 * @return mixed
	 */
	public static function getMediaLikes($variables)
	{
		return str_replace(['{variables}'], [$variables], self::MEDIA_LIKES_ENDPOINT);
	}

    /**
     * @param $variables
     * @return mixed
     */
    public static function getMediaComments($variables)
    {
        return str_replace(['{variables}'], [$variables], self::MEDIA_COMMENTS_ENDPOINT);
    }

    /**
     * @param $query
     * @return string|string[]
     */
	public static function topSearch($query)
	{
		return str_replace('{query}', urlencode($query), self::TOP_SEARCH_ENDPOINT);
	}
}