<?php namespace InstagramClient\components;

use InstagramClient\models\GenericModel;

/**
 * Trait PaginateResponse
 * @package InstagramClient\components
 */
trait PaginateResponse
{
	/**
	 * @var GenericModel[]
	 */
	protected $items = [];

	/**
	 * @var int
	 */
	protected $count = 0;

	/**
	 * @var int
	 */
	protected $total;
	/**
	 * @var string
	 */
	protected $cursor;

	/**
	 * @var bool
	 */
	protected $has_next_page;

	/**
	 * @param $items
	 * @param string $model
	 */
	private function setItems($items, $model = GenericModel::class)
	{
		$this->items = array_map(
			function ($item) use ($model) {
				return new $model($item);
			},
			$items
		);

		$this->count = count($items);
	}

	/**
	 * @param $total
	 */
	private function setTotal($total)
	{
		$this->total = intval($total);
	}

	/**
	 * @param $has_next_page
	 */
	private function setHasNextPage($has_next_page)
	{
		$this->has_next_page = boolval($has_next_page);
	}

	/**
	 * @param $cursor
	 */
	private function setCursor($cursor)
	{
		$this->cursor = strval($cursor);
	}

	/**
	 * @return int
	 */
	public function getTotal(): int
	{
		return $this->total;
	}

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		return $this->count;
	}

	/**
	 * @return string
	 */
	public function getCursor(): string
	{
		return $this->cursor;
	}

	/**
	 * @return bool
	 */
	public function hasNextPage(): bool
	{
		return $this->has_next_page;
	}

	/**
	 * @return bool
	 */
	public function hasItems()
	{
		return $this->items > 0;
	}

	/**
	 * @return GenericModel[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}
}