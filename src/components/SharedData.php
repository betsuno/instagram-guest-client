<?php namespace InstagramClient\components;

class SharedData
{
	protected $data = [];

	/**
	 * SharedData constructor.
	 * @param array $data
	 */
	public function __construct($data = [])
	{
		$this->data = $data;
	}

	/**
	 * @param $content
	 * @return mixed|null
	 */
	public static function extractFromContent($content)
	{
		if (preg_match_all('#\_sharedData \= (.*?)\;\<\/script\>#', $content, $out)) {
			return json_decode($out[1][0], true, 512, JSON_BIGINT_AS_STRING);
		}

		return null;
	}

	/**
	 * @param $content
	 * @return SharedData
	 */
	public static function createFromContent($content)
	{
		$sharedDataArray = static::extractFromContent($content);

		if (is_array($sharedDataArray)) {
			return new static($sharedDataArray);
		}

		return null;
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @return bool
	 */
	public function hasEntryData()
	{
		return isset($this->data['entry_data']);
	}

	/**
	 * @return mixed|null
	 */
	public function getEntryData()
	{
		return $this->hasEntryData() ? $this->data['entry_data'] : null;
	}

	/**
	 * @return bool
	 */
	public function hasRhxGis()
	{
		return isset($this->data['rhx_gis']);
	}

	/**
	 * @return mixed|null
	 */
	public function getRhxGis()
	{
		return $this->hasRhxGis() ? $this->data['rhx_gis'] : null;
	}

	/**
	 * @return bool
	 */
	public function hasCsrfToken()
	{
		return isset($this->data['config'], $this->data['config']['csrf_token']);
	}

	/**
	 * @return mixed|null
	 */
	public function getCsrfToken()
	{
		return $this->hasCsrfToken() ? $this->data['config']['csrf_token'] : null;
	}
}