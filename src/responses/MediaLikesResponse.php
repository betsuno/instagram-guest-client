<?php namespace InstagramClient\responses;

use InstagramClient\components\PaginateResponse;
use InstagramClient\models\UserItem;

/**
 * Class HashtagFeedResponse
 * @package InstagramClient\responses
 *
 * @method UserInfoResponse[] getItems()
 */
class MediaLikesResponse extends BaseResponse
{
	use PaginateResponse;

	/**
	 * @param $attributes
	 */
	public function __construct($attributes)
	{
		parent::__construct($attributes);
		$data = $attributes['data']['shortcode_media']['edge_liked_by'];

		$items = array_map(function ($item) {
			$data = $item['node'];
			return [
				'id'                  => $data['id'],
				'username'            => $data['username'],
				'full_name'           => $data['full_name'],
				'profile_picture_url' => $data['profile_pic_url'],
				'is_private'          => $data['is_private'],
			];
		}, $data['edges']);

		$this->setItems($items, UserItem::class);
		$this->setTotal($data['count']);
		$this->setCursor($data['page_info']['end_cursor']);
		$this->setHasNextPage($data['page_info']['has_next_page']);
	}
}