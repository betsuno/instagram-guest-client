<?php namespace InstagramClient\responses;

use InstagramClient\components\PaginateResponse;
use InstagramClient\models\Media;

/**
 * Class HashtagFeedResponse
 * @package InstagramClient\responses
 *
 * @method Media[] getItems()
 */
class HashtagFeedResponse extends BaseResponse
{
	use PaginateResponse;

	/**
	 * @param $attributes
	 */
	public function __construct($attributes)
	{
		parent::__construct($attributes);

		$items = array_map(function ($item) {
			$data = $item['node'];

			return [
				'id'                        => $data['id'],
				'caption'                   => isset($data['edge_media_to_caption']['edges'][0]['node']['text']) ? $data['edge_media_to_caption']['edges'][0]['node']['text'] : null,
				'type'                      => Media::extractType($data),
				'comments_count'            => $data['edge_media_to_comment']['count'],
				'likes_count'               => $data['edge_media_preview_like']['count'],
				'is_caption_edited'         => null,
				'thumbnail_url'             => $data['thumbnail_resources'][count($data['thumbnail_resources']) - 1]['src'],
				'high_resolution_image_url' => $data['display_url'],
				'link'                      => sprintf('https://www.instagram.com/p/%s/', $data['shortcode']),
				'short_code'                => $data['shortcode'],
				'created_at'                => $data['taken_at_timestamp'],
				'video_url'                 => isset($data['video_url']) ? $data['video_url'] : null,
				'owner'                     => $data['owner'],
                'album_items'               => Media::extractSlides($data)
			];
		}, $attributes['data']['hashtag']['edge_hashtag_to_media']['edges']);

		$this->setItems($items, Media::class);
		$this->setTotal($attributes['data']['hashtag']['edge_hashtag_to_media']['count']);
		$this->setCursor($attributes['data']['hashtag']['edge_hashtag_to_media']['page_info']['end_cursor']);
		$this->setHasNextPage($attributes['data']['hashtag']['edge_hashtag_to_media']['page_info']['has_next_page']);
	}
}