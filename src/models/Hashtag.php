<?php namespace InstagramClient\models;

/**
 * Class Hashtag
 * @package InstagramClient\models
 *
 * @method bool hasId()
 * @method bool hasName()
 * @method bool hasMediaCount()
 * @method bool hasUseDefaultAvatar()
 * @method bool hasProfilePicUrl()
 * @method bool hasSearchResultSubtitle()
 *
 * @method int getId()
 * @method string getName()
 * @method int getMediaCount()
 * @method bool getUseDefaultAvatar()
 * @method string getProfilePicUrl()
 * @method string getSearchResultSubtitle()
 */
class Hashtag extends GenericModel
{
    protected $propertiesMap = [
        'id'                     => 'int',
        'name'                   => 'string',
        'media_count'            => 'int',
        'use_default_avatar'     => 'bool', // не факт
        'profile_pic_url'        => 'string',
        'search_result_subtitle' => 'string',
    ];
}